import axios from "axios";

export const resultUserRandom = async () => {
  try {
    const response = await axios.get(`https://randomuser.me/api/?results=500`);
    const data = response.data.results;
    return data;
  } catch (error) {
    console.error(error);
    return error;
  }
};
