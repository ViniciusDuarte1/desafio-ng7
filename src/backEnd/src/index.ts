import { AddressInfo } from "net";
import express from "express";
import { userRouter } from "./router/userRadom";
import cors from "cors"

const app = express();
app.use(cors())

app.use(express.json());

app.use("/cliente", userRouter)

const server = app.listen(3000, () => {
  if (server) {
    const address = server.address() as AddressInfo;
    console.log(`Servidor rodando em http://localhost:${address.port}`);
  } else {
    console.error(`Falha ao rodar o servidor.`);
  }
});
