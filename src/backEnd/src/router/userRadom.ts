import express from "express"
import { UserRadomController } from "../controller/UserRadomController"


export const userRouter = express.Router()

const userRadomController = new UserRadomController()

userRouter.get("/genero", (res, req) => userRadomController.customerByGender(res,req));
userRouter.get( "/idade", (res, req) => userRadomController.customerByAge(res,req));
userRouter.get( "/mulher", (res, req) => userRadomController.womanOverEighteen(res,req));