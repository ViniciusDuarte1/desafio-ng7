import { Request, Response } from "express";
import { resultUserRandom } from "../service/resultUserRandom";

export class UserRadomController {
  async customerByGender(req: Request, res: Response) {
    const dataUserRadom = await resultUserRandom();

    const genderMale = [];
    const genderFemale = [];
    const totalGender = [];

    for (let i = 0; i < dataUserRadom.length; i++) {
      if (dataUserRadom[i].gender === "male") {
        genderMale.push(dataUserRadom[i]);
      } else {
        genderFemale.push(dataUserRadom[i]);
      }
      totalGender.push(dataUserRadom[i].gender);
    }

    const percentageMale = (genderMale.length / totalGender.length) * 100;
    const percentageFemale = (genderFemale.length / totalGender.length) * 100;

    res.status(200).send({
      genderMale: genderMale.length,
      genderFemale: genderFemale.length,
      percentageMale: Number(percentageMale.toFixed(2)),
      percentageFemale: Number(percentageFemale.toFixed(2)),
    });
  }

  async customerByAge(req: Request, res: Response) {
    const dataUserRadom = await resultUserRandom();

    const ageEighteenBetweenTwentyFive = [];

    const ageTwentySixBetweenForty = [];

    const ageAboveForty = [];

    const total = dataUserRadom.length;

    for (let i = 0; i < dataUserRadom.length; i++) {
      if (dataUserRadom[i].dob.age > 18 && dataUserRadom[i].dob.age <= 25) {
        ageEighteenBetweenTwentyFive.push(dataUserRadom[i].dob.age);
      } else if (
        dataUserRadom[i].dob.age > 26 &&
        dataUserRadom[i].dob.age <= 40
      ) {
        ageTwentySixBetweenForty.push(dataUserRadom[i].dob.age);
      } else {
        ageAboveForty.push(dataUserRadom[i].dob.age);
      }
    }

    const percentaEighteenBetweenTwentyFive =
      (ageEighteenBetweenTwentyFive.length / total) * 100;

    const percentaTwentySixBetweenForty =
      (ageTwentySixBetweenForty.length / total) * 100;

    const percentaAboveForty = (ageAboveForty.length / total) * 100;

    res.status(200).send({
      ageEighteenBetweenTwentyFive: ageEighteenBetweenTwentyFive.length,
      ageTwentySixBetweenForty: ageTwentySixBetweenForty.length,
      ageAboveForty: ageAboveForty.length,
      total: dataUserRadom.length,

      percentaEighteenBetweenTwentyFive: Number(
        percentaEighteenBetweenTwentyFive.toFixed(2)
      ),
      percentaTwentySixBetweenForty: Number(
        percentaTwentySixBetweenForty.toFixed(2)
      ),
      percentaAboveForty: Number(percentaAboveForty.toFixed(2)),
    });
  }

  async womanOverEighteen(req: Request, res: Response) {
    const dataUserRadom = await resultUserRandom();

    const womanOverEighteenData = []

    for (let i = 0; i < dataUserRadom.length; i++) {
      if (dataUserRadom[i].dob.age > 18 && dataUserRadom[i].gender ==="female") {
        womanOverEighteenData.push(dataUserRadom[i]);
      } 
    }

    const percentageWomanOverEighteenData =
      (womanOverEighteenData.length / dataUserRadom.length) * 100;
    
    const percentaWomanSmallerEighteen = (percentageWomanOverEighteenData - 100) * -1;

    res.status(200).send({
      percentageWomanOverEighteenData: Number(percentageWomanOverEighteenData.toFixed(2)),
      percentaWomanSmallerEighteen: Number(percentaWomanSmallerEighteen.toFixed(2))
    });
  }
}
