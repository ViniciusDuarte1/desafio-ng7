import styled from "styled-components";

export const ContainerApp = styled.main`
  display: grid;
  grid-template-columns: 80px 1fr;
  margin: auto;
  margin-top: 3%;
  width: 90%;
  min-height: 100vh;
  background-color: #090e16;
  color: #fff;
  @media (max-width: 450px) {
    padding-right: 25px;
  }
`;

export const SectionCard = styled.section`
  display: grid;
  grid-template-columns: 220px 220px 220px 220px 220px;
  margin-left: 3%;
  margin-top: 3%;
  height: 220px;

  @media (max-width: 1115px) {
    grid-template-columns: 220px 220px 220px;
  }
  @media (max-width: 840px) {
    grid-template-columns: 200px 200px;
    gap: 1em;
    padding-bottom: 6em;
    margin-left: 6.85em;
  }

  @media (max-width: 450px) {
    grid-template-columns: 220px;
    gap: 1em;
    height: 450px;
  }
`;

export const SectionAnalysis = styled.section`
  display: grid;
  grid-template-columns: 1fr 1fr;
  margin-top: 3%;
  margin-left: 3%;
  div {
    @media (max-width: 550px) {
      margin: auto;
    }
  }
  @media (max-width: 1115px) {
    grid-template-columns: 1fr;
    gap: 24px;
  }
`;

export const SectionBarChart = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;

  @media (max-width: 1115px) {
    width: 55vw;
  }
`;

export const SectionChartToNumber = styled.section`
  display: grid;
  grid-template-columns: 240px 240px 240px 240px;
  padding-top: 3%;
  padding-left: 3%;
  width: 92%;
  height: 8em;
  margin-top: 2%;
  margin-left: 3%;
  margin-bottom: 2em;

  @media (max-width: 1115px) {
    grid-template-columns: 220px 220px 220px;
    padding-bottom: 6.85em;
    gap: 16px;
  }
  @media (max-width: 840px) {
    grid-template-columns: 200px 200px;
    gap: 1em;
    padding-bottom: 16em;
  }
  @media (max-width: 450px) {
    /* justify-content: center;
    align-items: center;
    grid-template-columns: 240px; */
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    align-items: center;
  }
`;
