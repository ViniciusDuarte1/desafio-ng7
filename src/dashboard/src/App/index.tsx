import GlobalStyles from "../GlobalStyled";
import { BarChart } from "../components/BarChart";
import BatteryChart from "../components/BatteryChart";
import { Card } from "../components/Card";
import Chart from "../components/Chart";
import { Header } from "../components/Header";
import { LineChart } from "../components/LineChart";
import { SideBar } from "../components/Sidebar";
import * as S from "./styled";

import PermIdentitySharpIcon from "@mui/icons-material/PermIdentitySharp";

import ImportContactsIcon from "@mui/icons-material/ImportContacts";
import { useEffect, useMemo, useState } from "react";
import {
  clientByAge,
  clientByGender,
  clientWomanOverEighteen,
} from "../service/clientByGender";
import { gender } from "../@types/gender";

function App() {
  const [male, setFamale] = useState<gender | any>();
  
  const [percentaAge, setPercentaAge] = useState<any>();
  
  const [percentaWoman, setPercentaWoman] = useState<any>();

  useMemo(() => {
    clientByGender(setFamale);
  }, []);

  useEffect(() => {
    clientByAge(setPercentaAge);
  }, []);

  useEffect(() => {
    clientWomanOverEighteen(setPercentaWoman);
  }, []);

  const percentageFemale = male?.percentageFemale ?? null;
  const percentageMale = male?.percentageMale ?? null;

  const percentaEighteenBetweenTwentyFive =
    percentaAge?.percentaEighteenBetweenTwentyFive ?? null;

  const percentaTwentySixBetweenForty =
    percentaAge?.percentaTwentySixBetweenForty ?? null;

  const percentaAboveForty = percentaAge?.percentaAboveForty ?? null;

  const percentageWomanOverEighteenData =
    percentaWoman?.percentageWomanOverEighteenData ?? null;

  return (
    <>
      <GlobalStyles />
      <S.ContainerApp>
        <div>
          <SideBar />
        </div>
        <div>
          <Header />

          <S.SectionCard>
            <Card text="Totais de mulheres" amount={male && male.genderFemale}>
              <PermIdentitySharpIcon />
            </Card>
            <Card  text="Totais de homens" amount={male && male.genderMale}>
              <PermIdentitySharpIcon />
            </Card>
            <Card text="Acima de 40 anos" amount= {percentaAge && percentaAge.ageAboveForty}>
              <PermIdentitySharpIcon />
            </Card>
            <Card text="Totais de usuários" amount={percentaAge && percentaAge.total}>
              <PermIdentitySharpIcon />
            </Card>
            <Card text="Entre 18 e 25 anos" amount={percentaAge && percentaAge.ageEighteenBetweenTwentyFive}>
              <ImportContactsIcon />
            </Card>
          </S.SectionCard>

          <S.SectionAnalysis>
            <div>
              <LineChart
                data={[
                  percentaEighteenBetweenTwentyFive,
                  percentaTwentySixBetweenForty,
                  percentaAboveForty,
                ]}
                // data2={[48]}
                labels={[
                  "Entre 18 a 25 anos",
                  "Entre 26 a 40 anos",
                  "Acima de 40 anos",
                ]}
              />
            </div>
            <S.SectionBarChart>
              <div>
                {/* Gráfico efeito pizza */}
                <BarChart title="Mulheres">
                  <Chart
                    title="Acima de 18 anos "
                    title2="Abaixo de 18 anos"
                    number={percentageWomanOverEighteenData}
                  />
                  
                </BarChart>
              </div>

              <div>
                {/* Gráfico barra */}
                <BatteryChart
                  data={[percentageMale, percentageFemale]}
                  labels={["Homens", "Mulheres"]}
                />
              </div>
            </S.SectionBarChart>
          </S.SectionAnalysis>

          <S.SectionChartToNumber>
            <Card text="Totais de mulheres" amount={male && male.genderFemale} />
            <Card text="Totais de homens" amount={male && male.genderMale} />
            <Card
              text="Mulheres acima de 18 anos"
              amount={
                percentaWoman &&
                percentaWoman.percentageWomanOverEighteenData + "%"
              }
            />
            <Card
              text="Mulheres abaixo de 18 anos"
              amount={
                percentaWoman &&
                percentaWoman.percentaWomanSmallerEighteen + "%"
              }
            />
          </S.SectionChartToNumber>
        </div>
      </S.ContainerApp>
    </>
  );
}

export default App;
