import { createGlobalStyle } from "styled-components";

const GlobalStyles = createGlobalStyle`
* {
    margin: 0;
    padding: 0;
}
  body {
    font-family: 'Montserrat', sans-serif;
    background-color: #090924;
    
    @media (max-width: 950px) {
    background-color: #090e16;
  }
  }

`;

export default GlobalStyles;
