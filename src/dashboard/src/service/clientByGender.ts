import axios from "axios";
import { gender } from "../@types/gender";

export const clientByGender = (
  setGender: React.Dispatch<React.SetStateAction<gender>>
) => {
  axios
    .get(`http://localhost:3000/cliente/genero`)
    .then((res) => {
      setGender(res.data);
    })
    .catch((error) => {
      console.log(error);
    });
};

export const clientByAge = (setPercentaAge: React.Dispatch<any>) => {
  axios
    .get(`http://localhost:3000/cliente/idade`)
    .then((res) => {
      setPercentaAge(res.data);
    })
    .catch((error) => {
      console.log(error);
    });
};

export const clientWomanOverEighteen = (
  setPercentaWoman: React.Dispatch<any>
) => {
  axios
    .get(`http://localhost:3000/cliente/mulher`)
    .then((res) => {
      setPercentaWoman(res.data);
    })
    .catch((error) => {
      console.log(error);
    });
};
