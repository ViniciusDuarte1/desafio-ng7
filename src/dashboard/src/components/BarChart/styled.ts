import styled from "styled-components"


export const Container = styled.div`
   width: 100%;
   height: 12em;
   background:#15152b;
   display: flex;
   align-items: center;
   justify-content: center;
   @media (max-width: 1115px) {
    display: flex;
    flex-wrap: wrap;
   
  }
   h1 {
    font-family: Raleway;
    font-style: "boud";
    font-size: 22px;
    line-height: 16px;
    line-height: 100%;
    color: #c5ced9;
    font-size: none;
    padding-left: 5%;
    padding-bottom: 2%;
    padding-top: 2%;
  }
`