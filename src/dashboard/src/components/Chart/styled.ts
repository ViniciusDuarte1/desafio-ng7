import styled from "styled-components";

export const Container = styled.div`
  width: 280px;
  height: 180px;
  margin-top: -0.6em;

  @media (max-width: 950px) {
    width: 150px;
    height:150px;
   
  }
  @media (max-width: 817px) {
    width: 140px;
    height:110px;
   
  }
`;