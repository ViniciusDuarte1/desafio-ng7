import * as S from "./styled";

import { Doughnut } from "react-chartjs-2";

import { Tooltip, Chart as NewChart, Title, ArcElement } from "chart.js";

NewChart.register(Title, Tooltip, ArcElement);

interface ChartProps {
  number: number;
  title: string;
  title2: string;
}

function Chart({ number, title, title2 }: ChartProps) {
  const testCalcuate =  100 -  number ;
  
  return (
    <S.Container>
      <Doughnut
        width={200}
        height={200}
        options={{
          maintainAspectRatio: false,
          plugins: {
            title: {
              display: false,
              text: `${title}`,
            },
          },
        }}
        data={{
          labels: [title, title2],
          datasets: [
            {
              label: `${number}% a ${testCalcuate}%`,
              data: [number , testCalcuate],
              backgroundColor: ["#cfcccc", "#26ff2dce"],
              borderAlign: "center",
              borderWidth: 0.2,
              hoverBorderWidth: 0.8,
            },
          ],
        }}
      />
    </S.Container>
  );
}

export default Chart;
