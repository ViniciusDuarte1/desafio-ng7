import React, { useEffect, useRef } from "react";
import Chart from "chart.js/auto";
import * as S from "./styled";

interface BatteryChartProps {
  data: number[];
  labels: string[];
}

export const LineChart: React.FC<BatteryChartProps> = ({ data,  labels }) => {
  const chartRef = useRef<HTMLCanvasElement | null>(null);
  const chartInstance = useRef<Chart | null>(null);

  useEffect(() => {
    if (chartRef.current) {
      const ctx = chartRef.current.getContext("2d");

      if (ctx) {
        // Destruir o gráfico anterior, se existir
        if (chartInstance.current) {
          chartInstance.current.destroy();
        }

        // Criar o novo gráfico
        chartInstance.current = new Chart(ctx, {
          type: "line",
          data: {
            labels: labels,

            datasets: [
              {
                label: "Faixa de idade",
                data: data,
                backgroundColor: ["rgb(30, 82, 82)"],
                borderColor: "rgba(54, 162, 235, 1)",
                borderWidth: 0,
                fill: true,
              },

            ],
          },
          options: {
            // indexAxis: "y",
            scales: {
              x: {
                // beginAtZero: true,
                display: true, // Exibir o eixo x
                title: {
                  display: true,
                  text: "",
                },
              },
              y: {
                // beginAtZero: true,
                display: true, // Exibir o eixo y
                title: {
                  display: true,
                  text: "", // Título do eixo y
                },
              },
            },
          },
        });
      }
    }
  }, [data,  labels]);

  return (
    <S.Container>
      <canvas ref={chartRef} />
    </S.Container>
  );
};
