import styled from "styled-components";

export const Container = styled.div`
  width: 89%;
  height: 20em;
  background: #15152b;

  @media (max-width: 1115px) {
    width: 55vw;
  }

  @media (max-width: 450px) {
    width: 100vw;
    
  }
`;
