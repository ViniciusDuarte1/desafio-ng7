import * as S from "./styled";

import { ReactNode } from "react"

interface Props {
  children?: ReactNode;
  text: string;
  amount: string;
}

export const Card = ({ children, text, amount }: Props) => {
  return (
    <S.Container>
      <section>{children}</section>

      <S.Paragraph>
        <span>{text}</span>
        <div>
          <strong>{amount}</strong>
        </div>
      </S.Paragraph>
    </S.Container>
  );
};
