export interface gender {
  genderFemale: number;
  genderMale: number;
  percentageFemale: number;
  percentageMale: number;
}
