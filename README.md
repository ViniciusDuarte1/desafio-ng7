<h1 id="decisao">👨‍💻 Projeto Frontend: Desafio NG7 </h2>
<p> </p>
</h1>

<p align="center">
  <a href="#decisao">Decisões</a> &#xa0;|&#xa0; 
  <a href="#organizacao">Organização</a> &#xa0;|&#xa0; 
  <a href="#funciona">O que funciona</a> &#xa0;|&#xa0;
  <a href="#instrucao">Instruções</a>  &#xa0;|&#xa0;
  <a href="#tec">Tecnologias</a>  &#xa0;|&#xa0;
  <a href="#desenvolvedor">Desenvolvedor</a> 
</p>

<h2 id="decisao">👨‍💻 Decisões </h2>
<p> Minha decisões foram baseadas em três tópicos:
    <br/>
        1- Fazer o layout estático da minha aplicação
    <br/>
        2- Entender e aplicar a biblioteca de gráfico (chartJS)
    <br/>
        3- Tratamento de dados + integração com o back end
    
</p>
<p>No primeiro momento eu pensei em desenvolver somente a parte estática da minha aplicação seguindo a sugestão do wireframe do readme do projeto. Ademais, passei a estudar a biblioteca de gráfico e implementei na minha aplicação e por seguinte fiz o tratamento de dados e integrei o front com o back end. </p>

<h2 id="organizacao">🧑‍💻 Organização do Código</h2>

* Componentes reutilizáveis 
* Seperarando responsabilidades no front e criando back para tratamento de dados
* Usando boas práticas de typescript
* Usando boas práticas do styled-components
* Focando na experiência do dev
* Fácil compreensão do código
* Fácil manutenção do código

<h2 id="funciona">🖊️ Funcionalidades do DashBoard</h2>

* Clientes por gênero 
* Clientes por idade
* Quais das mulheres possuem mais de 18 anos

<h2 id="instrucao">
⚙️
 Instruções para rodar aplicação localmente
</h2>

Abra um terminal da sua preferência e digita o seguinte comando:

```bash
git clone https://gitlab.com/ViniciusDuarte1/desafio-ng7.git
```

Acesse o diretório do back end da aplicação usando o comando

```bash
cd src/backEnd
```

Instale as dependências usando o seguinte comando:

```bash
npm install
```

Execute o comando

```
npm run dev 
```

Espere um momento e assim que o back end subir ele vai mostrar a seguinte mensagem: 

```bash
Servidor rodando em http://localhost:3000
```

Após isso, abra um novo terminal e navague até o diretório do Front end <strong>dashboard </strong> com o seguinte comando:

```bash
cd src/dashboard
```
Instale as dependências usando o seguinte comando:

```bash
npm install --legacy-peer-deps
```

Por fim, Execute o seguinte comando:

```
npm run dev 
```


<h2 id="tec"> 🛠 Tecnologias </h2>

Ferramentas usadas na construção do projeto:

* HTML5 
* CSS3
* React Template TS
* Styled-components
* Design System: Material UI
* ChartJS
* Integração com API
* Axios
* NodeJs


  <h2 id="desenvolvedor">👷🏻‍♂ Desenvolvedor</h2>
<table> 
<tr>
  <td align="center"><a href="https://github.com/ViniciusDuarte17"><img style="border-radius: 50%" src="https://user-images.githubusercontent.com/92999708/210431021-9923435c-eefe-4757-b8e2-e441910a4e88.png" width="100px" alt=""/>
 <br />
 <sub><b>Vinicius Duarte</b></sub></a> <a href="https://github.com/ViniciusDuarte17">👩🏻‍💻</a></td>
 </tr>
</table>

<a href="#decisao">Voltar para o topo 🔝</a>
